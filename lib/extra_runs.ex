defmodule ExtraRuns do
  def extra_runs_data_main(matches_path, delv_path, year) do
    match_ids = CSV.decode(File.stream!(matches_path), headers: true) |> Enum.to_list |> Enum.map(fn {:ok,map} -> map end) |> Enum.filter(fn x -> x["season"]=="2016" end) |> Enum.map(fn x -> x["id"] end)
    filtered_deliveries_map_list = CSV.decode(File.stream!(delv_path), headers: true) |> Enum.to_list |> Enum.map(fn {:ok,map} -> map end) |> Enum.filter(fn x -> x["match_id"] in match_ids end)
    ans_map = Map.new
    _get_conceded_runs(filtered_deliveries_map_list, ans_map)
  end
  defp _get_conceded_runs([], ans_map) do
    ans_map
  end
  defp _get_conceded_runs([head|tail], ans_map) do
    team = head["bowling_team"]
    extra_run = Integer.parse(head["extra_runs"]) |> Tuple.to_list |> Enum.at(0)
    extras_so_far = Map.get(ans_map, team)
    if extras_so_far do
      ans_map = Map.put(ans_map, team, extras_so_far+extra_run)
      _get_conceded_runs(tail, ans_map)
    else
      ans_map = Map.put_new(ans_map, team, extra_run)
      _get_conceded_runs(tail, ans_map)
    end
  end
end