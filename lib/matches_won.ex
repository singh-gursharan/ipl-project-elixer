defmodule MatchesWon do
  def matches_won_by_teams_main(matches_path) do
    keyword_list = CSV.decode(File.stream!(matches_path), headers: true) |> Enum.to_list
    map_list = for {:ok, map} <- keyword_list, do: map
    get_ans(map_list)
  end

  def get_ans(map_list) do
    ans_map = Map.new
    _get_ans(map_list, ans_map)
  end

  defp _get_ans([], ans_map) do
    ans_map
  end

  defp _get_ans([head | tail], ans_map) do
    team = head["winner"]
    year = head["season"]
    cond do
      !(team in Map.keys ans_map) ->
      (
      IO.puts "1"
      season = Map.new
      season = Map.put_new(season, year, 1)
      ans_map = Map.put_new(ans_map, team, season)
      IO.inspect ans_map
      _get_ans(tail, ans_map)
      )
      ((team in Map.keys ans_map) && !(year in Map.keys Map.get(ans_map, team))) ->
      (
        IO.puts "2"
        team_map = ans_map |> Map.get(team) |> Map.put_new(year,1)
        ans_map = Map.put(ans_map, team, team_map)
        IO.inspect ans_map
        _get_ans(tail, ans_map)
      )
      true ->
      (
        IO.puts "3"
        matches_won_no = get_in(ans_map, [team,year])
        ans_map = put_in(ans_map, [team,year], matches_won_no+1)
        IO.inspect ans_map
        _get_ans(tail, ans_map)
      )
    end
  end
end