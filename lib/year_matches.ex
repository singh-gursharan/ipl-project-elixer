defmodule YearMatches do
  def no_of_matches_per_year_main(matches_path) do
    map_list = CSV.decode(File.stream!(matches_path), headers: true) |> Enum.to_list |> Enum.map(fn {:ok,map} -> map end)
    #map_list = for {:ok, map} <- keyword_list, do: map
    get_matches(map_list)
  end
  def get_matches(map_list) do
    ans_map = Map.new
    _get_matches(map_list, ans_map)
  end

  defp _get_matches([],ans_map), do: ans_map
  defp _get_matches([head|tail], ans_map) do
    season=head["season"]
    a=Map.fetch(ans_map,season) # if key not present will give nil
    ans_map = populate_map(a,ans_map, season)
    _get_matches(tail,ans_map)
  end
  defp populate_map(:error, ans_map, season) do
    ans_map = Map.put_new(ans_map, season, 1)
  end
  defp populate_map(_, ans_map, season) do
    matches = Map.get(ans_map,season)
    ans_map = Map.put(ans_map, season, matches+1)
  end
end