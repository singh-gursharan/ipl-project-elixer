import YearMatches
import MatchesWon
import ExtraRuns
import EconomicBowlers
defmodule App do
  @moduledoc """
  Documentation for `App`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> App.hello()
      :world

  """
  def hello do
    :world
  end
  @path1 "./assets/matches.csv"
  @path2 "./assets/deliveries_test.csv"
  def main do
    IO.puts "no of matches per year"
    YearMatches.no_of_matches_per_year_main(@path1)
    IO.puts "matches won by teams"
    MatchesWon.matches_won_by_teams_main(@path1)
    IO.puts "extra runs data"
    ExtraRuns.extra_runs_data_main(@path1, @path2, "2016")
    IO.puts "economic bowlers"
    EconomicBowlers.economic_bowlers_main(@path1, @path2, "2015")
  end
  
end
