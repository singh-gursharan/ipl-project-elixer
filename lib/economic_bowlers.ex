defmodule EconomicBowlers do
  def economic_bowlers_main(matches_path, delv_path, year) do
    match_ids = CSV.decode(File.stream!(matches_path), headers: true) |> Enum.to_list |> Enum.map(fn {:ok,map} -> map end) |> Enum.filter(fn x -> x["season"]=="2016" end) |> Enum.map(fn x -> x["id"] end)
    filtered_deliveries_map_list = CSV.decode(File.stream!(delv_path), headers: true) |> Enum.to_list |> Enum.map(fn {:ok,map} -> map end) |> Enum.filter(fn x -> x["match_id"] in match_ids end)
    ans_map = Map.new
    bowler_data = Map.new
    bowler_data = get_bowler_data(filtered_deliveries_map_list, bowler_data)
    ans_map = get_economy(bowler_data, ans_map)
  end
  defp get_bowler_data([], bowler_data) do
    bowler_data
  end
  defp get_bowler_data([head|tail], bowler_data) do
    bowler = head["bowler"]
    total_runs = Integer.parse(head["total_runs"]) |> Tuple.to_list |> Enum.at(0)
    wide = Integer.parse(head["wide_runs"]) |> Tuple.to_list |> Enum.at(0)
    noball = Integer.parse(head["noball_runs"]) |> Tuple.to_list |> Enum.at(0)
    delivery = if (wide>0 || noball>0), do: 0, else: 1

    if bowler in Map.keys bowler_data do
      runs_so_far = get_in(bowler_data, [bowler, "runs"])
      bowler_data = put_in(bowler_data, [bowler, "runs"], runs_so_far+total_runs)
      deliveries_so_far = get_in(bowler_data, [bowler, "deliveries"])
      bowler_data = put_in(bowler_data, [bowler, "deliveries"], deliveries_so_far+delivery)
      get_bowler_data(tail, bowler_data)
    else
      map = Map.new
      map = Map.put_new(map, "runs", total_runs) |> Map.put_new("deliveries",delivery)
      bowler_data = bowler_data |> Map.put_new(bowler,map)
      get_bowler_data(tail, bowler_data)
    end
  end
  defp get_economy(bowler_data,ans_map) do
    for {key, value} <- bowler_data do
      IO.inspect value
      economy = ((value["deliveries"]*100/6)/value["runs"])
      ans_map=Map.put_new(ans_map, key, economy)
    end
  end
end